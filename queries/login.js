const pool = require("../db").pool;
const knex = require("../db").knex;

module.exports.list_by_user = function(user_id) {
    return pool.fetchAll(`
        SELECT * FROM logins
        WHERE board_user=$1
        ORDER BY modified_at DESC
    `, [user_id]);
}

module.exports.count_online_since = async function(since) {
    const res = await knex("logins").where("modified_at", ">", since.format()).count();
    if (res.length > 0) {
        return res[0].count;
    }
    return 0;
}

module.exports.remove = function (id) {
    return pool.query(`
        DELETE FROM logins WHERE id=$1
    `, [id]);
}