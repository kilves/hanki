const pool = require("../db").pool;

module.exports.list = async function () {
    return pool.fetchAll(`
        SELECT * FROM embed_sources;
    `)
};

module.exports.find = function(id) {
    return pool.fetchOne(`
        SELECT * FROM embed_sources WHERE id=$1
    `, [id]);
}