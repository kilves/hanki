const knex = require("../db").knex;

function user_query() {
    return knex("users").whereNull("deleted_at");
}

module.exports.find_by_username = function(username) {
    return user_query().where("username", username).first();
};

module.exports.find_by_id = async function(id) {
    return user_query().where("id", id).first();
};

module.exports.count_online_since = async function(since) {
    const res = await user_query()
        .where("last_load", ">", since.format())
        .count();
    if (res.length > 0) {
        return res[0].count;
    }
    return 0;
}