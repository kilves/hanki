const {knex, pool} = require("../db");

function query() {
    return knex("attachments");
}

module.exports.find_by_post = async function(post_id) {
    return query().where("post", post_id);
}