const knex = require("../db").knex;

module.exports.find_by_thread_and_user = function(thread_id, user_id) {
    return knex("replied_threads")
        .where("thread", thread_id)
        .andWhere("board_user", user_id);
};
