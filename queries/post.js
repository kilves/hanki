const knex = require("../db").knex;
const pool = require("../db").pool;
const config = require("../config");
const moment = require("moment");

function select_thread() {
    return `
        SELECT p.*, b.url AS board_url, b.name AS board_name
        FROM posts AS p
    `;
}

function join_boards() {
    return `
            INNER JOIN boards AS b ON (b.id=p.board)
    `;
}

function thread_conditions() {
    return `
        WHERE p.deleted_at IS NULL
        AND p.thread IS NULL
        AND p.deleted_at IS NULL
        AND p.id NOT IN (SELECT h.post FROM hides AS h WHERE h.board_user=$1)
    `;
}

function thread_pagination() {
    return `
        ORDER BY p.sticky DESC, p.bump_time DESC
        OFFSET $2
        LIMIT $3
    `;
}

function thread_query() {
    return `
        ${select_thread()}
        ${join_boards()}
        ${thread_conditions()}
        ${thread_pagination()}
    `;
}

let post_query = function() {
    return knex("posts").whereNull("posts.deleted_at");
};

module.exports.list_user_threads = async function(user_id, page) {
    return pool.fetchAll(`
        ${select_thread()}
        ${join_boards()}
        AND p.board_user=$1
        ${thread_conditions()}
        ${thread_pagination()}
    `, [user_id, config.threads_per_page * page, config.threads_per_page]);
};

module.exports.list_replied_threads = async function(user_id, page) {
    return pool.fetchAll(`
        ${select_thread()}
        ${join_boards()}
        INNER JOIN replied_threads ON p.id=replied_threads.thread
        ${thread_conditions()}
        AND replied_threads.board_user=$1
        ${thread_pagination()}
    `, [user_id, config.threads_per_page * page, config.threads_per_page]);
};

module.exports.list_all_threads = async function(user_id, page) {
    return pool.fetchAll(`
        ${select_thread()}
        ${join_boards()}
        INNER JOIN boards ab ON ((ab.hidden=false OR ab.hidden IS NULL) AND p.board=ab.id)
        ${thread_conditions()}
        ${thread_pagination()}
    `, [user_id, config.threads_per_page * page, config.threads_per_page])
}

module.exports.list_board_threads = async function(board_id, user_id, page) {
    return pool.fetchAll(`
        ${select_thread()}
        ${join_boards()}
        ${thread_conditions()}
        AND p.board=$4
        ${thread_pagination()}
    `, [user_id, config.threads_per_page * page, config.threads_per_page, board_id]);
};

module.exports.find_by_id = function(id) {
    return post_query()
        .where("id", id)
        .first();
};

module.exports.delete = function(id) {
    const now = moment().format();
    return post_query()
        .where("id", id)
        .update({
            deleted_at: now
        });
}

module.exports.list_hidden_posts = async function(user_id) {
    return pool.fetchAll(`
        SELECT hides.*, posts.subject, posts.text FROM hides
        INNER JOIN posts ON (posts.id = hides.post AND hides.board_user=$1)
        WHERE posts.deleted_at IS NULL
    `, [user_id]);
};

module.exports.list_replies = function(thread_ids, user_id=null) {
    return pool.fetchAll(`
        SELECT * FROM posts 
        WHERE thread = ANY ($1)
        AND posts.deleted_at IS NULL
        AND posts.id NOT IN (SELECT hides.post FROM hides WHERE hides.board_user=$2)
        ORDER BY id
    `, [thread_ids, user_id]);
};

module.exports.count_replies = async function(thread_id) {
    return pool.count(`
        SELECT COUNT(*) FROM posts
        WHERE thread = $1
        AND posts.deleted_at IS NULL
    `, [thread_id]);
}

module.exports.bump_thread = async function(thread_id) {
    return post_query()
        .where("id", thread_id)
        .update("bump_time", knex.fn.now())
        .then(() => {});
};

module.exports.find_latest_post = function(user_id) {
    return post_query()
        .where("board_user", user_id)
        .orderBy("created_at", "desc")
        .first();
};

module.exports.find_latest_thread = function(user_id) {
    return module.exports.find_latest_post(user_id)
        .whereNull("thread");
};

module.exports.set_thread_stick = function(thread_id, value) {
    return post_query()
        .where("id", thread_id)
        .whereNull("thread")
        .update({ sticky: !!value });
}

module.exports.add_modinfo = function(post_id, modinfo) {
    return post_query()
        .where("id", post_id)
        .update({ modinfo: modinfo });
}