const pool = require("../db").pool;

module.exports.list_by_board = async function(board_id) {
    return pool.fetchAll("SELECT * FROM report_reasons WHERE board=$1 OR board IS NULL", [board_id]);
};

module.exports.create = async function(board_id, name, description) {
    return pool.query(`
        INSERT INTO report_reasons(board, name, description)
        VALUES($1, $2, $3)
    `, [board_id, name, description]);
};

module.exports.find_by_id = async function(id) {
    return pool.fetchOne("SELECT * FROM report_reasons WHERE id=$1 LIMIT 1", [id])
};

module.exports.delete = async function(id) {
    return pool.query("DELETE FROM report_reasons WHERE id=$1", [id]);
};