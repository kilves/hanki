const {pool} = require("../db");
const knex = require("../db").knex;

module.exports.insert = async function (data) {
    let permission = await knex("permissions").where({
        permission: data.permission,
        board_user: data.board_user,
        board: data.board
    }).first();
    if (permission) {
        return;
    }
    return knex("permissions").insert(data);
};

module.exports.delete_by_user_and_board = function (permission, user_id, board_id) {
    return knex("permissions")
        .where("board_user", user_id)
        .where("board", board_id)
        .where("permission", permission)
        .delete();
};

module.exports.is_admin = async function (user_id) {
    return !!(await knex("permissions")
        .where("board_user", user_id)
        .where("permission", "admin")
        .first());
}

module.exports.has_permission = async function (user_id, board_id, permission) {
    const perm = await pool.fetchOne(`
        SELECT * FROM permissions
        WHERE board_user=$1
        AND ((board=$2 AND permission=$3) OR permission='admin')
    `, [user_id, board_id, permission]);
    return !!perm;
};

module.exports.list_permissions = async function (user_id, board_id) {
    const results = await pool.fetchAll(`
        SELECT * FROM permissions
        WHERE board_user=$1
        AND (board=$2 OR permission='admin')
    `, [user_id, board_id]);
    return results.map(permission => permission.permission);
}