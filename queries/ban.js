const knex = require("../db").knex;
const pool = require("../db").pool;
const moment = require("moment");
const crypto = require("crypto");
const config = require("../config");

module.exports.list_for_mod = async function(board_id) {
    const now = moment().format();
    return pool.fetchAll(`
        SELECT bans.*, users.username AS banned_by FROM bans
        JOIN users ON users.id=bans.banned_by
        WHERE bans.board = $1
        AND bans.expires > $2
    `, [board_id, now]);
};

async function getValidBans(board_id, user_id, ip_plain) {
    const ip = crypto.createHash("sha256").update(ip_plain + config.ip_salt).digest("base64");
    const now = moment().toDate();
    return pool.fetchAll(`
        SELECT * FROM bans b WHERE
        (b.board=$1 OR b.board IS NULL OR b.global=TRUE)
        AND (board_user=$2 OR ip=$3)
        AND expires > $4
    `, [board_id, user_id, ip, now]);
}

module.exports.is_banned = async function(board_id, user_id, ip_plain) {
    const bans = await getValidBans(board_id, user_id, ip_plain);
    return bans.length > 0;
}

module.exports.get_ban_message = async function(board_id, user_id, ip_plain) {
    const bans = await getValidBans(board_id, user_id, ip_plain);
    return bans.length > 0 && bans[0].reason;
}

module.exports.add_ban = async function(
    id, ip, ip_plain, post_id, banned_by_id, board_id, expires, reason="Banned", note="", global = false) {
    return knex("bans")
        .insert({
            board_user: id,
            ip: ip,
            ip_plain: ip_plain,
            banned_by: banned_by_id,
            board: board_id,
            reason: reason,
            post: post_id,
            note: note,
            expires: expires.format(),
            global
        });
}