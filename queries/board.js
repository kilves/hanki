const {knex, pool} = require("../db");
const moment = require("moment");

let board_query = function() {
    return knex("boards").whereNull("boards.deleted_at");
};

module.exports.find_by_id = function(id) {
    return board_query()
        .andWhere("id", id)
        .first();
};

module.exports.find_by_url = function(url) {
    return board_query()
        .andWhere("url", url)
        .first();
};

module.exports.delete = function(board_id) {
    const now = moment().format();
    return board_query()
        .where("id", board_id)
        .update({
            deleted_at: now
        });
}

module.exports.list_by_owner = async function(user_id) {
    return pool.fetchAll(`
        SELECT * FROM boards WHERE board_user=$1 AND deleted_at IS NULL
    `, [user_id]);
}

module.exports.list = function () {
    return board_query()
        .where("hidden", false)
        .orWhereNull("hidden")
        .select("id", "url", "name")
        .orderBy("board_order", "asc");
};

module.exports.find_by_post_id = function(post_id) {
    return board_query()
        .select("boards.*")
        .innerJoin("posts", function() {
            this.on("posts.board", "boards.id")
                .andOnNull("posts.deleted_at")
        })
        .where("posts.id", post_id)
        .first();
};

module.exports.update = async function(board_id, values) {
    return board_query()
        .where("id", board_id)
        .update(values);
};

module.exports.count_posts = async function(board_id) {
    let count = await knex("posts").whereNull("deleted_at")
        .where("board", board_id)
        .count();
    return +count[0].count;
};

module.exports.increment_postcount = async function(board) {
    let count = await module.exports.count_posts(board.id);
    await knex("boards").where("id", board.id).update({
        postcount: count
    });
};
