const knex = require("../db").knex;
const TABLE = "reports";

module.exports.list_by_board = async function(board_id) {
    return knex(TABLE).whereNull("deleted_at").where("board", board_id);
};
