class BoardError extends Error {
    constructor(userMessage, cause) {
        super(userMessage);
        this.cause = cause;
    }
}

module.exports.BoardError = BoardError;