const moment = require("moment");

moment.fn.db = moment.fn.format.bind(this, "YYYY-MM-DDTHH:mm:ssZ");

module.exports = moment;
