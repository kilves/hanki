const MS_IN_SECOND = 1000;
const MS_IN_MINUTE = MS_IN_SECOND * 60;
const MS_IN_HOUR = MS_IN_MINUTE * 60;
const MS_IN_DAY = MS_IN_HOUR * 24;
const MS_IN_MONTH = MS_IN_DAY * 60;
const MS_IN_YEAR = MS_IN_DAY * 365;

module.exports = {
    MS_IN_SECOND,
    MS_IN_MINUTE,
    MS_IN_HOUR,
    MS_IN_DAY,
    MS_IN_MONTH,
    MS_IN_YEAR
};