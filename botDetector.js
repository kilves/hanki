const block_list = [
    "petalbot",
    "alittle client",
    "chrome privacy preserving prefetch proxy",
    "expanse, a palo alto",
    "dotbot",
    "apache-httpclient",
    "netsystemsresearch",
]

module.exports.botDetector = function (req, res, next) {
    const sourceLower = req.useragent.source.toLowerCase();
    for (let blocked of block_list) {
        if (sourceLower.includes(blocked)) {
            req.useragent.isBot = true;
            return next();
        }
    }
    next();
}