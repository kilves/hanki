const config = require("./config");
const pg = require("pg");
const process = require("process");
const Pool = pg.Pool;

module.exports.knex = require("knex")({
    client: "pg",
    connection: {
        host: process.env.PGHOST,
        port: process.env.PGPORT,
        database: process.env.PGDATABASE,
        user: process.env.PGUSER,
        password: process.env.PGPASSWORD
    },
    debug: config.debug
});

// numeric
pg.types.setTypeParser(1700, function (value) {
    return parseFloat(value);
});

// bigint
pg.types.setTypeParser(20, function (value) {
    return parseInt(value);
});

module.exports.pool = new Pool();

module.exports.pool.fetchOne = async function(query, params) {
    const res = await this.query(query, params);
    if (res.rows.length === 0) {
        return;
    }
    return res.rows[0];
};

module.exports.pool.fetchAll = async function(query, params) {
    const res = await this.query(query, params);
    return res.rows;
};

module.exports.pool.count = async function(query, params) {
    const res = await this.query(query, params);
    if (res.rows.length > 0 && Number.isInteger(res.rows[0].count)) {
        return res.rows[0].count;
    }
}