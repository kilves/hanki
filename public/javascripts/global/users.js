$(document).on("click", ".tablink", function(event) {
    event.preventDefault();
    let tgt = $(event.target).attr("data-target");

    $(".tab").addClass("hidden");
    $(`.tab#${tgt}`).removeClass("hidden");

    $(".tablink").removeClass("active");
    $(`.tablink[data-target=${tgt}]`).addClass("active");
});

$(document).on("click", "a.unhide", function(event) {
    event.preventDefault();
    const target = $(event.currentTarget);
    const id = target.attr("data-post-id");
    $.post(`/post/${id}/unhide`, {id: id}, data => {
        $(`.hidden_thread[data-post-id=${id}]`).replaceWith(data);
    });
});
