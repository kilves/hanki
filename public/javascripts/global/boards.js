const DROPDOWN = "data-dropdown";
const DROPDOWN_VISIBLE = "data-dropdown-visible";

$(document).on("click", "a.thumb", function (event) {
    event.preventDefault();
    const target = $(event.currentTarget);
    const origImage = target.find(".orig-image");
    const thumbImage = target.find(".thumb-image");
    origImage.toggle();
    thumbImage.toggle();
});

$(document).on("click", "[data-delete]", (event) => {
    event.preventDefault();
    let tgt = $(event.target);
    $.ajax({
        url: tgt.attr("href"),
        type: "DELETE",
        success: result => {
            window.location.reload();
        }
    });
});

$(document).on("change", "[data-permission]", (event) => {
    event.preventDefault();
    let req = {
        board: +$(event.target).attr("data-board"),
        user: +$(event.target).attr("data-user"),
        permission: $(event.target).attr("data-permission"),
        value: event.target.checked ? "true" : "",
    };
    $.post("/permissions", req, () => {

    });
});

$(document).on("click", "[data-dropdown]", function (event) {
    event.stopPropagation();
    event.preventDefault();

    const dropdown = $(event.currentTarget.getAttribute(`${DROPDOWN}`));
    const visibleDropdowns = $(`[${DROPDOWN_VISIBLE}]`);
    const dropdownVisible = dropdown.is(`[${DROPDOWN_VISIBLE}]`);
    if (dropdownVisible) {
        dropdown.removeAttr(DROPDOWN_VISIBLE);
    } else {
        visibleDropdowns.removeAttr(DROPDOWN_VISIBLE);
        visibleDropdowns.toggle("slide", {direction: "up"})
        dropdown.attr(DROPDOWN_VISIBLE, "");
    }
    dropdown.toggle("slide", {direction: "up"});
});

$(document).on("click", ".nav-dropdown", event => {

});

// Handle thread expand and contract
$(document).on("click", ".expand", function (event) {
    event.preventDefault();
    let thread = $(event.target).parents(".thread");
    let id = +thread.attr("data-id");
    $.get(`/expand/${id}`, content => {
        thread.replaceWith(content);
    });
});

$(document).on("click", ".contract", function (event) {
    event.preventDefault();
    let thread = $(event.target).parents(".thread");
    let id = +thread.attr("data-id");
    $.get(`/contract/${id}`, content => {
        thread.replaceWith(content);
    });
});

//delete confirm
$(document).on("click", "#deleteboard", function (event) {
    event.preventDefault();
    const confirm = window.confirm("Are you sure you want to delete this board?");
    if (confirm) {
        $.ajax($(this).attr("href"), {method: "DELETE"});
    }
});

$(document).ready(function () {
    onHashChange();
    // update tooltip positions
    $(document).mousemove(function (event) {
        const tooltip = $(".singlepost-tooltip");
        const height = window.innerHeight - event.clientY;
        tooltip.css("top", "");
        tooltip.css("bottom", "");
        if (height > 300) {
            tooltip.css("top", event.clientY);
        } else {
            tooltip.css("bottom", height + 10);
        }
        tooltip.css("left", event.clientX);
    });
    // Show singlepost tooltip while hovering over quotes

    $(".reference").mouseenter(async event => {
        const id = $(event.target).html().slice(8);
        const tooltip = $(".singlepost-tooltip");
        const tooltipContent = tooltip.find(".tooltip-content");
        tooltip.css("display", "inline-block");
        const result = await fetch("/singlepost/" + id);

        tooltipContent[0].innerHTML = await result.text();
    });

    $(".reference").mouseout(function () {
        $(".singlepost-tooltip").css("display", "none");
    });

    // Limit post length
    $(".post").each(function () {
        var len = $(this).find("p").html().length;
        if (len >= 3000) {
            var newpost = $(this).find("p").html().slice(0, 2899);
            var restofpost = $(this).find("p").html().slice(2900);
            newpost += '<div class="expandPost"><a href="">click here to show full post (' + String(restofpost.length) + ' remain)</a></div>';
            newpost += '<span class="hidden">' + restofpost + '</span>';
            $(this).find("p").html(newpost);
        }
    });
});

$(document).on("click", ".reference", async function (event) {
    event.preventDefault();
    const post_id = $(event.target).html().slice(8);
    const src = window.location.href;
    const url = new URL(src);
    url.pathname = "/jsrdr";
    url.hash = "";
    url.searchParams.append("id", post_id);
    url.searchParams.append("src", src);
    const result = await fetch(url.href);
    if (result.status === 404) {
        return;
    }
    const text = await result.text();

    const command = text.slice(0, 1);
    const fullUrl = new URL(src);
    const baseUrl = fullUrl.protocol + "//" + fullUrl.host + "/";
    if (command === "R") {
        //Redirect to ref
        window.location.href = baseUrl + text.slice(2);
    } else if (command === "S") {
        // Stay on page and highlight post
        window.location.href = window.location.href + "#" + post_id;
    } else {
        // Do nothing
    }
});

// Report post
$(document).on("click", ".report-post", async function (event) {
    event.preventDefault();
    const target = $(event.currentTarget);
    const post_id = +target.parents("[data-id]").attr("data-id");
    const board_url = target.attr("data-board-url");
    $(".report").css("display", "block");
    $(".ban").css("display", "none");
    const result = await fetch("/" + board_url + "/rules");
    const options = await result.text();
    $(".report #reason")[0].innerHTML = options;
    $("#report-post").find("#report-post-id").val(post_id);
});

// Ban post
$(document).on("click", ".ban-button", async function (event) {
    event.preventDefault();
    const target = $(event.currentTarget);
    const ban = $(".ban");
    const post_id = +target.attr("data-post-id");
    ban.find("#ban-post-id").val(post_id);
    ban.css("display", "block");
    $(".report").css("display", "none");
});

$(document).on("click", ".delete-button", async function (event) {
    event.preventDefault();
    const confirm = window.confirm("Are you sure you want to delete this post?");
    if (!confirm) {
        return;
    }
    const target = $(event.currentTarget);
    const post_id = +target.attr("data-post-id");
    const board_url = target.attr("data-board-url");
    $.ajax({
        url: "/" + board_url + "/" + post_id,
        type: "DELETE",
        success: result => {
            window.location.reload();
        }
    });
});

$(document).on("click", ".duration-buttons a", async function (event) {
    event.preventDefault();
    const seconds = +$(event.currentTarget).attr("data-duration");
    const duration = +$("#ban-duration").val();
    $("#ban-duration").val(Math.max(duration + seconds, 0));
});

/*
$(".reference").mouseenter(function() {
    t = $(this).parent("p").children(".singlepost-tooltip");
    alert("ok");
    if(t.length) {
      t.css("display", "block");
    } else {
      id = $(this).parent("div[id^=answer]").attr("id").slice(6);
      $(this).parent("p").append('<div class="singlepost-tooltip"></div>');
      $(this).parent("p").css("display", "block");
      alert("ok");
      $.get("/singlepost/"+id, function(data) {
        $(this)parent("p").children(".singlepost-tooltip").append(data);
      });
    }
  });
  
  $(".reference").mouseout(function() {
    $(this).parent("p").children(".reference-tooltip").css("display"; "none");
  });
 */

// Click to show all text
$(document).on("click", ".expandPost", function (event) {
    event.preventDefault();
    var t = $(event.target).parent().parent();
    t.find(".hidden").contents().unwrap();
    t.find(".expandPost").remove();
});

// Handle thread hiding
$(document).on("click", ".hide-thread", function (event) {
    event.preventDefault();
    let t = $(event.target);
    let id = +t.parents("[id^=answer]").attr("data-id");
    $.post(`/post/${id}/hide`, function (data) {
        let post_element = $(`#answer-${id}`);
        post_element.parent(".hide-replace").replaceWith(data);
    });
});

// Remove board from subscribed
$(document).on("click", ".unsub", function (event) {
    event.preventDefault();
    var t = $(event.target).parents(".ukkoElement");
    var id = t.attr("id");
    $.get("/unsubscribe", {id: id}, function (data) {
        t.toggle("fade")
        t.remove();
    });
});

// Add board to subscribed
$(document).on("click", ".sub", function (event) {
    event.preventDefault();
    var t = $(event.target).parents(".categoryElement");
    var addTo = $(".ukko");
    var id = t.attr("id");
    $.get("/subscribe", {id: id}, function (data) {
        $("#ukko").append(data);
        t.children(".sub").empty();
    });
});

// Selecting posts
$(document).on("click", "input[id^=select]", function (event) {
    var checked = $("input[id^=select]:checked");
    if (checked.length > 0) {
        $("#post-operations").css("display", "grid");
    } else {
        $("#post-operations").css("display", "none");
    }
});

$(document).on("click", "#select-all", function (event) {
    event.preventDefault();
    $("input[id^=select]").prop("checked", true);
    $(this).attr("id", "deselect-all");
    $(this).html("Deselect all");
});

$(document).on("click", "#deselect-all", function (event) {
    event.preventDefault();
    $("input[id^=select]").prop("checked", false);
    $(this).attr("id", "select-all");
    $(this).html("Select all");
});

$(document).on("click", "#post-delete", async function (event) {
    event.preventDefault();
    const post_ids = $("input[id^=select]:checked").map((i, e) => +e.id.slice(7)).get();
    await fetch("/deleteposts")
    /*
    var html = '<input class="temporary" type="hidden" name="posts" value="' + postids.join('-') + '">';
    $("#deleteposts").append(html);
    $("form#deleteposts").submit();
    $(".temporary").remove(); */
});

// SETTINGS N SHIT
$(document).on("change", ".settings-select", function () {
    var val = $(this).attr("id");
    if (val == "stylesheet") {
        $.get("/user/edit", {stylesheet: $(this).val()}, function (data) {
            $('link[id="dynamic-stylesheet"]').attr("href", data);
        });
    } else if (val == "post_noko") {
        $.get("/user/edit", {post_noko: $(this).val()});
    } else if (val == "locale") {
        $.get("/user/edit", {locale: $(this).val()});
    }
});

shitfunc = function (el) {
    return String($(el).is(":checked"));
};

function save_settings(e) {
    e.stopImmediatePropagation();
    let body = {};
    $(".user-input").each((_, element) => {
        let el = $(element);
        body[el.attr("name")] = el.is(":checked") ? "on" : "";
    });
    $(".user-input-select").each((_, element) => {
        let el = $(element);
        body[el.attr("name")] = el.find("option:selected").val();
    });
    $.post("/user/edit", body);
}

$(document).on("change", ".user-input", save_settings);
$(document).on("change", ".user-input-select", save_settings);

$(document).on("change", "#user_save_scroll", function () {
    $.post("/user/edit", {save_scroll: String($(this).is(":checked"))})
});
$(document).on("change", "#user_hide_names", function () {
    $.post("/user/edit", {hide_names: String($(this).is(":checked"))})
});
$(document).on("change", "#user_hide_region", function () {
    $.post("/user/edit", {hide_region: String($(this).is(":checked"))})
});
$(document).on("change", "#user_autoload_media", function () {
    $.post("/user/edit", {autoload_media: String($(this).is(":checked"))})
});
$(document).on("change", "#user_autoplay_gifs", function () {
    $.post("/user/edit", {autoplay_gifs: String($(this).is(":checked"))})
});

// download posts
$(document).on("click", "#image-download", function (event) {
    event.preventDefault();
    const post_ids = [];
    post_ids[0] = "download";
    $("input[id^=select]:checked").each(function (i, e) {
        post_ids[i + 1] = e.id.slice(7);
    });
    const name = $("#filename-field").val();
    const pids = post_ids.join('-');
    var html1 = '<input class="temporary" type="hidden" name="posts" value="' + pids + '">';
    var html2 = '<input class="temporary" type="hidden" name="filename" value="' + name + '">';
    $("#deleteposts").append(html1);
    $("#deleteposts").append(html2);
    $("form#deleteposts").submit();
    $(".temporary").remove();
});

function showElement(element, show) {
    if (show) {
        element.removeClass("invisible");
    } else {
        element.addClass("invisible");
    }
}

function toggleEmbed(fileContainer, open) {
    const video = fileContainer.find(".video");
    const image = fileContainer.find(".post-image");
    const play = fileContainer.find(".play-media");
    const bottomOptions = fileContainer.find(".bottom-options");
    showElement(video, open);
    showElement(image, !open);
    showElement(play, !open);
    showElement(bottomOptions, open);

    if (open) {
        video.removeClass("nothumb");
        video.attr("controls", "");

        if (video[0].paused) {
            video[0].play();
        } else {
            video[0].pause();
        }
    } else {
        video[0].currentTime = 0;
        video[0].pause();
        video.addClass("nothumb");
    }
}

function openEmbed(mediaContainer) {
    toggleEmbed(mediaContainer, true)
}

function closeEmbed(mediaContainer) {
    toggleEmbed(mediaContainer, false);
}

// media
$(document).on("click", ".media-container", function (event) {
    event.preventDefault();
    openEmbed($(event.currentTarget).parents(".filecontainer"));
});

$(document).on("click", ".filecontainer .exit", function (event) {
    event.preventDefault();
    const target = $(event.currentTarget);
    const parent = target.parents(".filecontainer");
    closeEmbed(parent);
});

function onHashChange() {
    const h = location.hash;
    const textarea = $("#textarea-message");
    if (h.substr(0, 3) === "#q_") {
        const message_id = h.substr(3);
        textarea.val(textarea.val() + ">>" + message_id + "\r\n");
        $("html,body").scrollTop(0);
    } else {
        const message_id = h.substr(1);
        const element = $("#answer-" + message_id);
        $(".highlight").attr("class", "answer");
        if (element[0] && element.attr("class") !== "first") {
            element.attr("class", "highlight");
            element[0].scrollIntoView();
        }
    }
}

$(window).hashchange(onHashChange);
$(document).on("click", ".quote-link", onHashChange);
