
const knex = require("./db").knex;
const moment = require("moment");
const post_queries = require("./queries/post");
const board_queries = require("./queries/board");
const uuid = require("uuid/v4");
const config = require("./config");
const login_queries = require("./queries/login");
const permission_queries = require("./queries/permission");
const {MS_IN_YEAR} = require("./util/math");
const {log} = require("debug");

async function _create_user(req, res) {
    const now = moment().format();
    let users = await knex("users").insert({
        online: true,
        last_load: now,
        created_at: now,
        modified_at: now,
        last_page: req.originalUrl,
        site_style: "default",
        show_sidebar: 1,
        show_postform: true,
        save_scroll: true,
        autoload_media: false,
        autoplay_gifs: false,
        sfw: false,
        locale: "fi-FI",
        timezone: "Europe/Helsinki",
        hide_names: false,
        hide_region: false,
        hide_ads: 0,
        hide_browserwarning: false,
        username: uuid(),
        password: "",
        password_salt: ""
    }).returning("*");

    if (!users.length > 0) {
        return users[0];
    }
    let user = users[0];

    knex("overboards").insert({
        board_user: user["id"],
    }).then(() => {});

    knex("replieds").insert({
        board_user: user["id"]
    }).then(() => {});

    let token = uuid();
    let series = uuid();

    knex("logins").insert({
        board_user: user["id"],
        browser: req.useragent,
        token: token,
        series: series
    }).then(() => {});
    res.cookie("hb_token", token, {signed: true, maxAge: MS_IN_YEAR * 20});
    res.cookie("hb_series", series, {signed: true, maxAge: MS_IN_YEAR * 20});

    return user;
}

function process_logins (logins, current_token) {
    return logins.map(login => {
        const browser = JSON.parse(login.browser);
        return {
            ...login,
            source: browser ? `${browser.browser}, ${browser.os}` : "Unknown",
            current: login.token === current_token
        };
    });
}

module.exports.base = async function(req, res, next) {
    req.data = {};
    req.data.title = config.title;
    req.data.boards = await board_queries.list(req, res);
    req.data.stats = {
        hourly_users: await login_queries.count_online_since(moment().subtract(1, "hours")),
        daily_users: await login_queries.count_online_since(moment().subtract(1, "days"))
    };
    if (req.useragent.isBot || req.useragent.isCurl) {
        res.locals.language = "fi";
        req.data.user = {};
        return next();
    }
    let users = [];
    if (req.signedCookies["hb_token"] && req.signedCookies["hb_series"]) {
        users = await knex("logins")
            .where("logins.token", req.signedCookies["hb_token"])
            .andWhere("logins.series", req.signedCookies["hb_series"])
            .join("users", {"users.id": "logins.board_user"});
    }
    if (users.length === 0) {
        req.data.user = await _create_user(req, res);
    } else {
        req.data.user = users[0];
        req.data.user[req.data.user.site_style] = true;
        req.data.user["return" + req.data.user.post_noko] = true;
        req.data.user[req.data.user.locale] = true;
        await knex("logins").where("token", req.signedCookies["hb_token"]).update({
            modified_at: moment().format(),
        });
    }
    req.data.user.hidden_threads = await post_queries.list_hidden_posts(req.data.user.id);
    req.data.user.logins = process_logins(await login_queries.list_by_user(req.data.user.id), req.signedCookies["hb_token"]);
    req.data.user.boards = await board_queries.list_by_owner(req.data.user.id);
    req.data.user.admin = await permission_queries.is_admin(req.data.user.id);
    res.locals.language = req.data.user.locale.split("-")[0];
    next();
};
