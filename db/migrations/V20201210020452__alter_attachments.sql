alter table attachments
    drop column file_meta,
    drop column file_file_name,
    drop column file_content_type,
    drop column file_file_size,
    drop column file_updated_at,
    add column media_picture_ext text;