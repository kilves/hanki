drop table permissions;
alter table board_permissions rename to permissions;
alter table permissions drop column uid;
alter table permissions add column board_user integer references users(id);
