create table users(
    id serial primary key,
    ip text,
    online boolean,
    last_load timestamp with time zone,
    last_page text,
    site_style text,
    show_sidebar integer,
    show_postform boolean,
    save_scroll boolean,
    sfw boolean,
    locale text,
    timezone text,
    post_password text,
    post_name text,
    post_noko integer,
    hide_names boolean,
    autoload_media boolean,
    autoplay_gifs boolean,
    hide_region boolean,
    hide_ads integer,
    hide_browserwarning boolean,
    username text,
    password text,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null,
    deleted_at timestamp with time zone,
    password_salt text,
    token text,
    series text,
    stylesheet text,
    chat_last timestamp with time zone,
    in_chat boolean
);

create table categories(
    id serial primary key,
    name text,
    board_order integer,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table boards(
    id serial primary key,
    url text,
    name text,
    description text,
    board_order integer,
    international boolean,
    pages integer,
    locked boolean,
    worksafe boolean,
    ad_category integer,
    namefield integer,
    default_name text,
    show_empty_names boolean,
    default_style text,
    hide_sidebar boolean,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null,
    category integer references categories(id),
    board_user integer references users(id),
    hidden boolean,
    postcount integer
);

create table embed_sources(
    id serial primary key,
    name text,
    video_url text,
    embed_code text,
    width integer,
    height integer,
    code text,
    help text,
    sfw boolean
);

create table posts(
    id serial8 primary key,
    board_user integer references users(id),
    ip text,
    ip_plain text,
    proxy boolean,
    geoip_country_code text,
    geoip_country_name text,
    geoip_region_code text,
    geoip_region_name text,
    geoip_city text,
    geoip_lat text,
    geoip_lon text,
    name text,
    tripcode text,
    posted_by_op boolean,
    posted_by_mod boolean,
    time timestamp with time zone,
    subject text,
    text text,
    password text,
    sage boolean,
    rage boolean,
    love boolean,
    noko boolean,
    bump_time timestamp with time zone,
    locked boolean,
    sticky boolean,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null,
    deleted_at timestamp with time zone,
    thread integer references posts(id),
    board integer references boards(id),
    attachments text,
    mod boolean,
    modinfo text
);

create table attachments(
    id serial primary key,
    name text,
    orig_name text,
    extension text,
    thumb_ext text,
    thumb_width integer,
    thumb_height integer,
    mime text,
    size integer,
    information text,
    md5 text,
    duplicate_of integer references attachments(id),
    id3_name text,
    id3_artist text,
    id3_length text,
    id3_butrate text,
    id3_image integer,
    folder text,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null,
    file_meta text,
    file_file_name text,
    file_content_type text,
    file_file_size integer,
    file_updated_at timestamp with time zone,
    post integer references posts(id)
);

create table attachments_posts(
    id serial primary key,
    attachment integer references attachments(id),
    post integer references posts(id)
);

create table bans(
    id serial primary key,
    board integer references boards(id),
    board_user integer references users(id),
    post integer references posts(id),
    expires timestamp with time zone,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null,
    ip text,
    ip_plain text,
    reason text,
    banned_by text,
    note text
);

create table board_permissions(
    id serial primary key,
    uid text,
    permission text,
    board integer references boards(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table overboards(
    id serial primary key,
    board_user integer references users(id),
    boards text,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table boards_overboards(
    board integer references boards(id),
    overboard integer references overboards(id)
);

create table frontpage_posts(
    id serial primary key,
    subject text,
    text text,
    board_user integer references users(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table hides(
    id serial primary key,
    post integer references posts(id),
    board_user integer references users(id),
    time timestamp with time zone,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table logins(
    id serial primary key,
    board_user integer references users(id),
    series text,
    token text,
    browser text,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table permissions(
    id serial primary key,
    permission text,
    board_user integer references users(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table replieds(
    id serial primary key,
    board_user integer references users(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table post_ids(
    id serial primary key,
    post integer references posts(id),
    replied integer references replieds(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table posts_replieds(
    id serial primary key,
    post integer references posts(id),
    replied integer references replieds(id)
);

create table report_reasons(
    id serial primary key,
    name text,
    description text,
    board integer references boards(id),
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table reports(
    id serial primary key,
    reason integer references report_reasons(id),
    description text,
    board integer references boards(id),
    post integer references posts(id),
    checked boolean,
    created_at timestamp with time zone not null,
    modified_at timestamp with time zone not null
);

create table threads(
    id serial primary key,
    board integer references boards(id)
);
