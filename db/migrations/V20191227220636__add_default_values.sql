alter table users alter column created_at set default now();
alter table users alter column modified_at set default now();

alter table categories alter column created_at set default now();
alter table categories alter column modified_at set default now();

alter table boards alter column created_at set default now();
alter table boards alter column modified_at set default now();

alter table posts alter column created_at set default now();
alter table posts alter column modified_at set default now();

alter table attachments alter column created_at set default now();
alter table attachments alter column modified_at set default now();

alter table bans alter column created_at set default now();
alter table bans alter column modified_at set default now();

alter table board_permissions alter column created_at set default now();
alter table board_permissions alter column modified_at set default now();

alter table overboards alter column created_at set default now();
alter table overboards alter column modified_at set default now();

alter table frontpage_posts alter column created_at set default now();
alter table frontpage_posts alter column modified_at set default now();

alter table hides alter column created_at set default now();
alter table hides alter column modified_at set default now();

alter table logins alter column created_at set default now();
alter table logins alter column modified_at set default now();

alter table permissions alter column created_at set default now();
alter table permissions alter column modified_at set default now();

alter table replieds alter column created_at set default now();
alter table replieds alter column modified_at set default now();

alter table post_ids alter column created_at set default now();
alter table post_ids alter column modified_at set default now();

alter table report_reasons alter column created_at set default now();
alter table report_reasons alter column modified_at set default now();

alter table reports alter column created_at set default now();
alter table reports alter column modified_at set default now();
