alter table bans drop column banned_by;
alter table bans add column banned_by integer references users(id);