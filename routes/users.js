const express = require('express');
const crypto = require("crypto");
const config = require("../config");
const {error} = require("../controllers/error");
const login_queries = require("../queries/login");
const {BoardError} = require("../util/error");
const {MS_IN_YEAR} = require("../util/math");
const router = require("express-promise-router")();
const knex = require("../db").knex;



router.get("/edit", async (req, res, next) => {
    req.redirect("/");
});

/* GET users listing. */
router.post('/edit', async (req, res, next) => {
    req.body.post_noko = [0, 1, 2].indexOf(+req.body.post_noko) !== -1 ? req.body.post_noko : 0;
    req.body.show_sidebar = [0, 1, 2].indexOf(+req.body.show_sidebar) !== -1 ? req.body.show_sidebar : 0;
    await knex("users").where("id", req.data.user.id).update({
        show_sidebar: req.body.show_sidebar,
        site_style: req.body.stylesheet,
        save_scroll: !!req.body.save_scroll,
        hide_names: !!req.body.hide_names,
        hide_region: !!req.body.hide_region,
        autoload_media: !!req.body.autoload_media,
        autoplay_gifs: !!req.body.autoplay_gifs,
        post_noko: req.body.post_noko,
        locale: req.body.locale
    });
    res.status(200).send();
});

router.get("/login", async (req, res, next) => {
    res.redirect("/");
});

router.post("/login", async (req, res, next) => {
    let user = await knex("users").where("username", req.body.username).first();
    if (!user) {
        return error(res, 403, "Wrong username or password!");
    }

    let [hash, salt] = user.password.split(":");

    crypto.scrypt(req.body.password, salt, 64, async (err, derivedKey) => {
        if (err) {
            throw new BoardError("Login failed", err);
        }
        if (hash !== derivedKey.toString("base64")) {
            return error(res, 403, "Wrong username or password!");
        }

        let token = crypto.randomBytes(64).toString("hex");
        let series = crypto.randomBytes(64).toString("hex");

        knex("logins").insert({
            board_user: user.id,
            browser: req.useragent,
            series: series,
            token: token,
        }).then(() => {
            res.cookie("hb_token", token, {signed: true, maxAge: MS_IN_YEAR * 20});
            res.cookie("hb_series", series, {signed: true, maxAge: MS_IN_YEAR * 20});
            res.redirect("back");
        }).catch(err => {
            if (err.code === "23505") {
                return error(res, 400, "This username is taken.");
            }
            return error(res, 400);
        });
    });
});

router.get("/logout/:id", async (req, res, next) => {
    const login = req.data.user.logins.find(login => login.id === +req.params.id);
    if (login) {
        await login_queries.remove(login.id);
    }
    res.redirect("back");
});

router.get("/logout", async (req, res, next) => {
    await knex("logins")
        .where("token", req.signedCookies["hb_token"])
        .delete();
    res.cookie("hb_series", null, {signed: true});
    res.cookie("hb_token", null, {signed: true});
    res.redirect("back");
});

router.post("/logout/:id", async (req, res, next) => {
    await knex("logins")
        .where("board_user", req.data.user.id)
        .andWhere("id", req.params.id)
        .delete();
    res.cookie("hb_series", null, {signed: true});
    res.cookie("hb_token", null, {signed: true});
    res.redirect("back");
});

router.get("/create", async (req, res, next) => {
    res.redirect("/");
});

function set_password(user, password, callback, username=null) {
    let salt = crypto.randomBytes(32).toString("base64");
    crypto.scrypt(password, salt, 64, (err, derivedKey) => {
        if (err) {
            return callback(err);
        }
        knex("users").where("id", user.id).update({
            username: !username ? user.username : username,
            password: `${derivedKey.toString("base64")}:${salt}`
        }).then(() => {
            callback(null, true);
        }).catch(e => {
            callback(err);
        });
    });
}

router.post("/create", async (req, res, next) => {
    if (req.data.user.password) {
        return res.redirect("/404");
    }

    if (req.body.password !== req.body.password_again) {
        return error(res, 400, "Passwords don't match!");
    }

    if (req.body.password.length > 64) {
        return error(res, 400, "Password is too long!");
    }

    set_password(req.data.user, req.body.password, (err, val) => {
        if (err) {
            error(res, 500);
            throw new BoardError("Operation failed", err);
        }
        res.redirect("back");
    }, req.body.username);
});

router.get("/changepassword", (req, res, next) => {
    res.redirect("/");
});

router.post("/changepassword", async (req, res, next) => {

    if (req.body.new_password !== req.body.new_password_again) {
        return error(res, 403, "Wrong password!");
    }

    let [hash, salt] = req.data.user.password.split(":");
    crypto.scrypt(req.body.password, salt, 64, (err, derivedKey) => {
        if (err) {
            error(res, 500);
            throw new BoardError("Operation failed", err);
        }

        if (derivedKey !== hash) {
            return error(res, 400, "Wrong password!");
        }

        set_password(req.data.user, req.body.new_password, (err, val) => {
            if (err) {
                error(res, 400, 500);
                throw new BoardError("Operation failed", err);
            }
            res.redirect("back");
        });
    });
});

module.exports = router;
