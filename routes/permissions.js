const express = require("express");
const board_queries = require("../queries/board");
const user_queries = require("../queries/user");
const permission_queries = require("../queries/permission");
const config = require("../config");
const router = require("express-promise-router")();

async function modify_permissions(board_id, user_id, permissions, value) {
    let permission_operations = [];
    for (let permission of config.permissions) {
        if (permissions.indexOf(permission) !== -1) {
            permission_operations.push(modify_permission(board_id, user_id, permission, value));
        }
    }
    return Promise.all(permission_operations);
}

async function modify_permission(board_id, user_id, permission, enabled) {
    if (enabled) {
        await permission_queries.insert({
            board: board_id,
            board_user: user_id,
            permission: permission
        });
    } else {
        await permission_queries.delete_by_user_and_board(permission, user_id, board_id);
    }
}

router.post("/", async (req, res, next) => {
    let board = await board_queries.find_by_id(+req.body.board);
    if (req.data.user.id !== board.board_user) {
        return res.status(401).send();
    }

    let user;

    if (req.body.username) {
        user = await user_queries.find_by_username(req.body.username);
    } else if (req.body.user) {
        user = await user_queries.find_by_id(req.body.user);
    } else {
        return res.status(400).send();
    }

    if (req.body.permission) {
        await modify_permission(board.id, user.id, req.body.permission, !!req.body.value);
    } else {
        const permissions = Object.getOwnPropertyNames(req.body)
            .filter(name => name.startsWith("perm-"))
            .map(name => name.replace("perm-", ""));
        await modify_permissions(board.id, user.id, permissions, true);
    }

    return res.redirect("back");
});

module.exports = router;
