const express = require("express");
const board_queries = require("../queries/board");
const rule_queries = require("../queries/rule");
const permission_queries = require("../queries/permission");
const knex = require("../db").knex;
const router = require("express-promise-router")();

router.delete("/:id", async (req, res, next) => {
    await knex("reports")
        .where("deleted_at", null)
        .andWhere("id", req.params.id)
        .innerJoin("permissions", function() {
            this.on("permissions.board", "reports.board").andOn("permissions.board_user", req.data.user.id);
        })
        .update({
            deleted_at: knex.fn.now()
        });
    res.status(200).send("");
});

router.post("/", async (req, res, next) => {
    const board = await board_queries.find_by_post_id(+req.body.post);
    const rule = rule_queries.find_by_id(req.body.reason);
    if (!rule || !board) {
        return res.redirect("back");
    }
    await knex("reports").insert({
        reason: req.body.reason,
        description: req.body.description,
        post: +req.body.post,
        board: board.id,
        global_rule: !rule.board
    });
    res.redirect("back");
});

router.get("/", async (req, res, next) => {
    const query = knex("reports")
        .select("reports.*")
        .select({
            board_url: "boards.url"
        })
        .where("reports.deleted_at", null)
        .join("boards", "boards.id", "reports.board");
    if (await permission_queries.is_admin(req.data.user.id)) {
        query.where("reports.global_rule", true);
    } else {
        query
            .whereNot("reports.global_rule", true)
            .whereIn("reports.board", q => {
                q.select("permissions.board")
                    .from("permissions")
                    .where("board_user", req.data.user.id)
            });
    }
    req.data.reports = await query;
    res.render("reports", req.data);
});

module.exports = router;
