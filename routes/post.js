const crypto = require("crypto");
const config = require("../config");
const path = require("path");
const post_controller = require("../controllers/post");
const post_queries = require("../queries/post");
const board_queries = require("../queries/board");
const replied_thread_queries = require("../queries/replied_thread");
const gm = require("gm");
const md5 = require("md5");
const id3 = require("node-id3");
const fs = require("fs");
const mime = require("mime-types");
const mp3Parser = require("mp3-parser");
const moment = require("moment");
const router = require("express-promise-router")();
const multer = require("multer");
const mm = require("music-metadata");
const ban_queries = require("../queries/ban");
const embed_source_queries = require("../queries/embed_source");
const {populate_permissions} = require("../controllers/board");
const {parse_message} = require("../controllers/post");
const {error} = require("../controllers/error");
const upload = multer();
const knex = require("../db").knex;
const cp = require("child_process");
const stream = require("stream");
const {BoardError} = require("../util/error");

async function can_post(user) {
    let limit = moment().subtract(...config.max_post_rate);
    let post = await post_queries.find_latest_post(user.id);
    if (!post) {
        return true;
    }
    let latest_post_time = moment(post.created_at);
    return latest_post_time.isBefore(limit);
}

async function can_post_thread(user) {
    let limit = moment().subtract(...config.max_thread_rate);
    let post = await post_queries.find_latest_thread(user.id);
    if (!post) {
        return true;
    }
    let latest_post_time = moment(post.created_at);
    return latest_post_time.isBefore(limit);
}

function parse_name(post_name, board) {
    if (!post_name || !board.namefield) {
        return [];
    }
    let [name, trip] = post_name.split("#");
    let hash = crypto.createHash("sha256").update(trip).digest("base64");
    if (board.namefield === 1) {
        name = null;
    }
    return [name, hash];
}

/**
 *
 * @param dataBuffer
 * @return {Promise<{image, width, height, thumb, thumb_width, thumb_height, thumb_ext}>}
 */
async function process_image(dataBuffer) {
    return new Promise((resolve, reject) => {
        let image = dataBuffer, width, height, thumb_width, thumb_height;
        gm(dataBuffer).selectFrame(0).resize(200, 200).setFormat("webp").quality(50).toBuffer((err, thumbBuffer) => {
            if (err) {
                return reject(err);
            }
            gm(dataBuffer).size((err, originalSize) => {
                if (err) {
                    return reject(err);
                }
                width = originalSize.width;
                height = originalSize.height;
                gm(thumbBuffer).size((err, thumbSize) => {
                    if (err) {
                        return reject(err);
                    }
                    thumb_width = thumbSize.width;
                    thumb_height = thumbSize.height;
                    resolve({ image, width, height, thumb: thumbBuffer, thumb_width, thumb_height, thumb_ext: "webp" });
                });
            });
        });
    });
}

async function process_mp3(data) {
    return new Promise((resolve, reject) => {
        id3.read(data, (err, tags) => {
            if (err) {
                return reject();
            }
            let header = mp3Parser.readFrameHeader(data);
            resolve({...tags, bitrate: header.bitrate, });
        });
    });
}

/**
 *
 * @param video_buffer
 * @return {Promise<{data: Buffer, format: string}>}
 */
async function extract_first_frame(video_buffer) {
    return new Promise(((resolve, reject) => {
        fs.writeFileSync("/tmp/pohjoislauta_tmp", video_buffer);
        const proc = cp.spawn("ffmpeg", [
            "-i", "/tmp/pohjoislauta_tmp",
            "-vf", "scale=w=320:h=240:force_original_aspect_ratio=decrease",
            "-loglevel", "quiet",   // Nothing extra to stdout
            "-vframes", "1",        // Extract first frame
            "-f", "image2pipe",     // Format to pipe
            "-c:v", "png",          // Output as jpg
            "pipe:"                 // Output to stdout
        ]);
        let buffers = [];

        proc.on("spawn", () => {
            console.log("process spawned");
        });
        proc.stdout.on("data", data => {
            buffers.push(data);
        });
        proc.stdout.on("error", error => {
            console.error("STDOUT ERROR");
            reject(error);
        })
        proc.on("close", code => {
            if (code !== 0) {
                reject("Failed to extract first frame", code);
            }
            resolve({
                format: "image/jpeg",
                data: Buffer.concat(buffers)
            });
        });
        proc.on("error", err => {
            console.error("PROC ERROR");
            reject(err);
        });
        proc.stdin.on("error", error => {
            console.error("STDIN ERROR");
            reject(error);
        });
        proc.stderr.on("data", data => {
            console.error(data.toString());
        });
        proc.stdin.write(video_buffer);
        proc.stdin.destroy();
    }));
}

/**
 *
 * @param data
 * @return {Promise<{artist: string, year: *, length: *, bitrate: number, title: string, picture: {data: Buffer, format: string}[]}>}
 */
async function process_media(data) {
    const metadata = await mm.parseBuffer(data.buffer, data.mimetype);
    const meta = {
        title: metadata.common.title,
        artist: metadata.common.artist,
        bitrate: metadata.format.bitrate,
        length: metadata.format.duration,
        year: metadata.format.year,
        picture: metadata.common.picture
    }

    if (!meta.picture && data.mimetype.startsWith("video")) {
        meta.picture = [await extract_first_frame(data.buffer)];
    }
    return meta;
}

async function save_attachment_file(id, name, file) {
    let save_dir = path.join(process.cwd(), "files", `${id}`);
    fs.mkdirSync(save_dir, { recursive: true });
    fs.writeFileSync(path.join(save_dir, name), file);
}

async function create_attachments(post, post_id) {
    for (let attachment of post.attachments) {
        const imageParts = attachment.originalname.split(".");
        if (!imageParts || imageParts.length < 2) {
            throw new BoardError(`${attachment.originalname} is not a valid file name`);
        }
        const extension = imageParts[imageParts.length - 1];
        if (config.extensionWhiteList.indexOf(extension) === -1) {
            throw new BoardError(`${extension} is not an allowed file format`);
        }

        let duplicate = await knex("attachments").where({md5: md5(attachment.buffer)}).first();
        const is_image = attachment.mimetype.startsWith("image/");
        const is_video = attachment.mimetype.startsWith("video/");
        const is_audio = attachment.mimetype.startsWith("audio/");
        let media_picture = null;

        let time = Math.floor(new Date().getTime() / 1000);
        let rand = Math.floor(Math.random() * 100000);
        let name = `${time}${rand}`;

        if (duplicate) {
            await knex("attachments").insert({
                post: post_id,
                orig_name: attachment.originalname,
                name: name,
                extension: duplicate.extension,
                thumb_ext: duplicate.thumb_ext,
                thumb_width: duplicate.thumb_width,
                thumb_height: duplicate.thumb_height,
                mime: duplicate.mime,
                size: duplicate.size,
                information: duplicate.information,
                md5: duplicate.md5,
                id3_name: duplicate.id3_name,
                id3_artist: duplicate.id3_artist,
                id3_length: duplicate.id3_length,
                id3_bitrate: duplicate.id3_bitrate,
                media_picture_ext: duplicate.media_picture_ext,
                duplicate_of: duplicate.id,
            });
            return;
        }
        let thumb_ext = extension;

        let image_data;
        let media_data;
        let media_picture_data;

        let file_buffer;
        let thumb_buffer;
        let media_picture_buffer;
        let media_picture_ext;


        if (is_image) {
            image_data = await process_image(attachment.buffer);

            thumb_buffer = image_data.thumb;
            thumb_ext = image_data.thumb_ext;
            file_buffer = image_data.image;
        } else if (is_audio || is_video) {
            media_data = await process_media(attachment);
            media_picture = media_data.picture ? media_data.picture[0] : null;
            media_picture_data = await process_image(media_picture.data);

            file_buffer = attachment.buffer;

            media_picture_buffer = media_picture_data.image;
            media_picture_ext = "jpg";

            thumb_buffer = media_picture_data.thumb;
            thumb_ext = media_picture_data.thumb_ext;

        }

        let inserted_attachments = await knex("attachments").insert({
            post: post_id,
            orig_name: attachment.originalname,
            name: name,
            extension: extension,
            thumb_ext: thumb_ext,
            media_picture_ext: media_picture_ext,
            mime: attachment.mimetype,
            size: attachment.size,
            md5: md5(attachment.buffer),
            thumb_width: image_data ? image_data.thumb_width : null,
            thumb_height: image_data ? image_data.thumb_height : null,
            duplicate_of: duplicate ? duplicate.id : null,
            information: is_image ? `${image_data.width}x${image_data.height}` : null,
            id3_name: media_data ? media_data.title : null,
            id3_artist: media_data ? media_data.artist : null,
            id3_length: media_data ? media_data.length : null,
            id3_bitrate: media_data ? media_data.bitrate : null
        }).returning("id");

        const id = inserted_attachments[0]["id"];

        // Save the actual file
        await save_attachment_file(
            id,
            `orig.${extension}`,
            file_buffer,
        );
        if (thumb_buffer) {
            await save_attachment_file(
                id,
                `thumb.${thumb_ext}`,
                thumb_buffer
            );
        }
        if (media_picture_buffer) {
            await save_attachment_file(
                id,
                `media.${media_picture_ext}`,
                media_picture_buffer
            );
        }
    }
}

function shorten(text) {
    if (!text) {
        return "";
    }
    let short_text = text.substring(0, 40);
    if (short_text.length === 40) {
        short_text += "...";
    }
    return short_text;
}

async function create_post(req, board, thread, post, user) {

    let [tripname, tripcode] = parse_name(post.name, board);
    let embed_source = await knex("embed_sources").where("id", post.embed_source || null).first();
    let embed_code = post.embed_code;

    // Allow youtube links
    if (embed_code && embed_source && embed_source.code.includes("youtube")) {
        const embedUrl = new URL(embed_code);
        if (embedUrl.hostname.endsWith("youtube.com")) {
            embed_code = embedUrl.searchParams.get("v");
        }
        if (embedUrl.hostname.endsWith("youtu.be")) {
            const [_, code] = embedUrl.pathname.split("/");
            embed_code = code;
        }
    }
    let inserted_post = await knex("posts").insert({
        board: post.board,
        thread: post.thread,
        board_user: user.id,
        subject: post.subject || shorten(post.message),
        text: parse_message(post.message),
        mod: post.mod,
        ip: crypto.createHash("sha256").update(req.ip + config.ip_salt).digest("base64"),
        ip_plain: req.ip,
        embed_code: embed_code,
        embed_source: post.embed_code ? post.embed_source : null,
        posted_by_op: thread ? thread.board_user === user.id && post.posted_by_op : true,
        posted_by_mod: post.posted_by_mod,
        time: knex.fn.now(),
        name: tripname,
        tripcode: tripcode,
        locked: false,
        sticky: false,
        sage: post.sage,
        rage: post.rage,
        love: post.love,
        bump_time: post.thread ? null : knex.fn.now()
    }).returning("id");

    const post_id = inserted_post[0]["id"];

    try {
        await create_attachments(post, post_id);
    } catch (e) {
        await knex("posts").where("id", post_id).delete();
        await knex("attachments").where("post", post_id).delete();
        console.error("Error creating attachments");
        throw e;
    }

    if (!post.sage && thread) {
        await post_queries.bump_thread(thread.id);
    }

    board_queries.increment_postcount(board).then(() => {});
}

async function add_to_replieds(user, thread) {
    if (!thread) {
        return;
    }
    let replieds = await replied_thread_queries.find_by_thread_and_user(thread.id, user.id);
    if (replieds.length === 0) {
        await knex("replied_threads").insert({
            thread: thread.id,
            board_user: user.id
        });
    }
}

router.post("/:id/hide", async (req, res, next) => {
    req.data.layout = false;
    let id = +req.params.id;
    await post_controller.hide(id, req.data.user);
    res.render("hidden", req.data);
});

router.post("/:id/unhide", async (req, res, next) => {
    req.data.layout = false;
    let id = +req.params.id;
    await post_controller.unhide(id, req.data.user);
    res.send();
});

router.get("/create", async (req, res) => {
    res.redirect("/");
});

function is_empty(message) {
    return !message
        .replace(/\s/g, "")
        .replace(/[\u200B-\u200D\uFEFF]/g, "")
        .replace(/[\u0000-\u001f\u007f-\u009f]/g, "")
        .replace(/[\u0600-\u0605\u061c\u06dd]/g, "")
        .length
}

router.post("/create", upload.any(), async (req, res) => {

    if (req.body.email || req.body.firstname || req.body.lastname || req.body.website) {
        res.redirect("/");
        return;
    }

    let post = {
        thread: +req.body["thread_id"] || null,
        message: req.body["message"],
        subject: req.body["subject"],
        attachments: req.files ? req.files : [],
        embed_code: req.body["embed_code"],
        name: req.body["name"],
        posted_by_op: !!req.body["posted_by_op"],
        embed_source: req.body["embed_source"],
        board: +req.body["board_id"],
        sage: !!req.body["sage"],
        rage: !!req.body["rage"],
        love: !!req.body["love"],
        noko: !!req.body["noko"]
    };

    await populate_permissions(req, +req.body.board_id);

    post.posted_by_mod = !!req.body.posted_by_mod && req.data.user.mod;
    post.mod = !!req.body.posted_by_admin && req.data.user.admin;

    if (post.thread === 0 && !post.message) {
        return error(res, 400, "Message is required for new threads");
    }

    if (is_empty(post.message) && (!post.embed_code && post.attachments.length === 0)) {
        return error(res, 400, "Either an attachment or a message is required");
    }

    if (!(await can_post(req.data.user)) && !req.data.user.admin) {
        return error(res, 429, "You are posting too fast!");
    }

    if (post.thread === null && !(await can_post_thread(req.data.user)) && !req.data.user.admin) {
        return error(res, 429, "You are creating threads too fast!");
    }

    let board = await board_queries.find_by_id(post.board);

    let thread = await post_queries.find_by_id(post.thread);

    if (thread) {
        const reply_count = await post_queries.count_replies(thread.id);
        if (reply_count >= config.max_thread_replies) {
            return error(res, 400, `Thread has reached reply limit (${config.max_thread_replies})`);
        }
    }

    if (!board) {
        return error(res, 404, "Could not find board.");
    }

    if (thread && thread.locked) {
        return error(res, 400, "Thread is locked.");
    }

    if (post.attachments.length + !!post.embed_source > 4) {
        return error(res, 400, "You are only allowed to post a maximum of 4 attachments or embeds.");
    }

    if (post.embed_source) {
        const embed_source = await embed_source_queries.find(post.embed_source);
        if (!embed_source) {
            return error(res, 404, "Embed source not found.");
        }
    }

    if (await ban_queries.is_banned(board.id, req.data.user.id, req.ip)) {
        return error(res, 400, "You are banned. Nice try though for getting here. You have been awarded a cookie. <!-- Nothing actually happened. Thanks, and have fun. -->");
    }

    await create_post(req, board, thread, post, req.data.user);
    await add_to_replieds(req.data.user, thread);

    if (post.noko) {
        res.redirect(`/${board.url}/${post.thread}`);
    } else if (!req.data.user.post_noko) {
        res.redirect(`/${board.url}`);
    } else if (req.data.user.post_noko === 1) {
        res.redirect(`/${board.url}/${post.thread}`);
    } else if (req.data.user.post_noko === 2) {
        res.redirect("/ukko");
    }
});

module.exports = router;
