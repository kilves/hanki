const board_queries = require("../queries/board");
const post_queries = require("../queries/post");
const user_queries = require("../queries/user");
const moment = require("moment");
const ban_queries = require("../queries/ban");
const permission_queries = require("../queries/permission");
const post_route = require("./post");
const {parse_message} = require("../controllers/post");
const router = require("express-promise-router")();

router.post("/", async (req, res) => {
    const {
        post_id,
        reason,
        duration,
        note
    } = req.body;
    const post = await post_queries.find_by_id(post_id);
    if (!post) {
        return res.status(404).send("");
    }
    const board = await board_queries.find_by_id(post.board);
    if (!board) {
        return res.status(404).send("");
    }
    const can_ban = await permission_queries.has_permission(req.data.user.id, board.id, "ban");
    const can_ban_globally = await permission_queries.is_admin(req.data.user.id);
    if (!can_ban) {
        return res.status(403).send("");
    }
    const duration_seconds = +duration;
    if (!Number.isFinite(duration_seconds)) {
        return res.status(400).send("");
    }
    const user = await user_queries.find_by_id(post.board_user);

    const expires = moment().add(duration_seconds, "seconds");
    if (!expires.isValid()) {
        return res.status(400).send("");
    }
    const target_is_admin = await permission_queries.is_admin(user.id);
    if (!target_is_admin) {
        await ban_queries.add_ban(
            user ? user.id : null,
            post.ip,
            post.ip_plain,
            post.id,
            req.data.user.id,
            board.id,
            expires,
            reason,
            note,
            can_ban_globally && !!req.body["ban_global"],
        );
    }

    const visible_reason = parse_message(reason) || "User was banned for this post";

    await post_queries.add_modinfo(
        post_id,
        '<span class="banned">( '+ visible_reason +' )</span>'
    );
    return res.redirect("back");
});

module.exports = router;