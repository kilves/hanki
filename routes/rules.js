const express = require("express");
const router = require("express-promise-router")();
const rule_queries = require("../queries/rule");
const permission_queries = require("../queries/permission");

router.post("/", async (req, res, next) => {
    let perm = await permission_queries.has_permission(req.data.user.id, req.body.board, "change_rules");

    if (!perm) {
        return res.status(401).redirect("back");
    }
    await rule_queries.create(req.body.board, req.body.name, req.body.description);
    res.redirect("back");
});

router.delete("/:id", async (req, res, next) => {
    let rule = await rule_queries.find_by_id(req.params.id);
    if (!rule) {
        return res.status(404).send("");
    }
    const is_admin = await permission_queries.is_admin(req.data.user.id);
    if (!rule.board && is_admin) {
        await rule_queries.delete(req.params.id);
        return res.status(200).send("");
    } else if (!is_admin) {
        return res.status(403).send("");
    }
    let perm = await permission_queries.has_permission(req.data.user.id, rule.board, "change_rules");
    if (!perm) {
        return res.status(403).send("");
    }
    await rule_queries.delete(req.params.id);
    res.status(200).send("");
});

module.exports = router;
