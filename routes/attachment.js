const router = require("express-promise-router")({
    mergeParams: true
});
const path = require("path");
const fs = require("fs").promises;
const SortedSet = require("collections/sorted-set");
const {not_found} = require("../controllers/error");
const knex = require("../db").knex;

const MAX_ATTACHMENT_CACHE_SIZE = 1073741824; // 1 Gigabyte
const attachment_cache = {};
let cached_attachment_names = new SortedSet();
let attachment_cache_size = 0;

function is_cached(attachment_name) {
    return cached_attachment_names.has(attachment_name);
}

async function cache(attachment_name) {
    let attachment = await knex("attachments").where("name", attachment_name).first();
    if (attachment.duplicate_of) {
        attachment = await knex("attachments").where("id", attachment.duplicate_of).first();
    }
    return update_cache(attachment);
}

async function update_cache(attachment) {
    const new_attachment = await knex("attachments").where("id", attachment.id).first();

    const styles = {
        "orig": () => `orig.${new_attachment.extension}`,
        "thumb": () => `thumb.${new_attachment.thumb_ext}`,
        "media": () => `media.${new_attachment.media_picture_ext}`
    }

    // Retreive files
    const aid = new_attachment.id.toString();
    const styleNames = Object.getOwnPropertyNames(styles);
    for (let style of styleNames) {
        try {
            const file = await fs.readFile(path.join(process.cwd(), "files", aid, styles[style]()));
            new_attachment[`${style}_file`] = file;
            if (!is_cached(new_attachment.name)) {
                attachment_cache_size += file.length;
            }
        } catch (err) {
            if (err.code !== "ENOENT") {
                throw err;
            }
        }
    }

    if (attachment_cache_size > MAX_ATTACHMENT_CACHE_SIZE) {
        // Cache too big! Free first attachment
        const first_attachment = cached_attachment_names.shift();
        for (let style of styleNames) {
            const file = first_attachment[`{style}_file`];
            if (!file) {
                continue;
            }
            attachment_cache_size -= file.length;
        }
        delete attachment_cache[first_attachment.name];
    }

    // Add to cache
    cached_attachment_names.push(new_attachment.name);
    attachment_cache[new_attachment.name] = new_attachment;
    return new_attachment;
}

async function retreive(attachment_name) {
    if (!is_cached(attachment_name)) {
        return cache(attachment_name);
    }
    const attachment = attachment_cache[attachment_name];
    update_cache(attachment).then(() => {});
    return attachment;
}

function parseRange(range, file) {
    const [method, rangeStr] = range.split("=");
    const [startStr, endStr] = rangeStr.split("-");
    let start = parseInt(startStr);
    let end = parseInt(endStr);
    if (startStr === "") {
        start = 0;
    }
    if (endStr === "") {
        end = file.length - 1;
    }
    return { start, end };
}

router.use(async (req, res, next) => {
    if (!req.path.startsWith("/files/")) {
        return next();
    }
    const match = req.path.match(/^\/files\/(thumb|orig|media)\/(\d+)+\/(.+)$/);
    if (!match) {
        return not_found(req, res);
    }
    const [full_name, style, name, orig_name] = match;

    let attachment = await retreive(name);
    if (!attachment || ["orig", "thumb", "media"].indexOf(style) === -1) {
        return res.status(404).send();
    }

    if (style === "thumb" && attachment.thumb_ext === null) {
        return res.status(404).send();
    }

    const file = attachment[`${style}_file`];
    if (!file) {
        return res.status(404).send();
    }

    const range = req.header("Range");
    if (range) {
        const { start, end } = parseRange(range, file);
        if (start < 0 || end >= file.length) {
            return res.status(416).send("");
        }
        res.status(206);
        res.set({
            "Accept-Ranges": "bytes",
            "Content-Range": `bytes ${start}-${end}/${file.length}`,
        });
        res.send(file.slice(start, end + 1));
    } else {
        res.send(file);
    }
});

module.exports = router;
