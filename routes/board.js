const post_controller = require("../controllers/post");
const post_queries = require("../queries/post");
const permission_queries = require("../queries/permission");
const rule_queries = require("../queries/rule");
const report_queries = require("../queries/report");
const ban_queries = require("../queries/ban");
const permissions = require("../controllers/permission");
const board_queries = require("../queries/board");
const config = require("../config");
const embed_source_queries = require("../queries/embed_source");
const {populate_permissions} = require("../controllers/board");
const router = require("express-promise-router")();

async function get_board(url, page, user) {
    if (url === "mythreads") {
        return post_controller.get_my_threads(page, user);
    } else if (url === "repliedthreads") {
        return post_controller.get_replied_threads(page, user);
    } else if (url === "ukko") {
        return post_controller.get_all_threads(page, user);
    } else {
        return post_controller.get_board_threads(url, page, user);
    }
}

function build_nav(page, max_pages) {
    let nav = {
        next: page + 2,
        previous: page - 0,
        first: page === 0,
        last: page + 1 === max_pages,
        pages: []
    };

    let p = 1;
    for (let i = 1; i < 11; i++) {
        let this_page = Math.max(p + page - 5, p)
        nav.pages.push({
            page: this_page,
            current: this_page === page + 1
        });
        p++;
    }

    return nav;
}

router.use("/:board*", async (req, res, next) => {
    const [board_url, page] = req.params.board.split("-");
    const board = await board_queries.find_by_url(board_url);
    if (!board) {
        return next();
    }
    req.data.embed_sources = await embed_source_queries.list();
    req.data.user.banned = await ban_queries.is_banned(board.id, req.data.user.id, req.ip);
    if (req.data.user.banned) {
        req.data.user.ban_message = await ban_queries.get_ban_message(board.id, req.data.user.id, req.ip);
    }
    await populate_permissions(req, board.id);
    next();
})

router.get('/:board', async (req, res, next) => {
    let [board_url, page] = req.params["board"].split("-");
    page = parseInt(page, 10) - 1 || 0;
    req.data.board = await get_board(board_url, page, req.data.user);
    req.data.nav = build_nav(page);
    req.data.supported_formats = config.extensionWhiteList;
    if (req.data.board) {
        req.data.report_reasons = await rule_queries.list_by_board(req.data.board.id);
        res.render("board/show", req.data);
    } else {
        res.status(404).render("404", req.data);
    }
});

async function render_thread(req, res, full) {
    let thread = await post_controller.get_thread(+req.params.id, req.data.user.id, full);
    req.data.layout = false;
    req.data.threads = [thread];
    await populate_permissions(req, thread.board.id);
    res.render("expand", req.data);
}

router.get("/expand/:id", async (req, res, next) => {
    await render_thread(req, res, true);
});

router.get("/contract/:id", async (req, res, next) => {
    await render_thread(req, res, false);
});

router.get("/:board/mod", async (req, res, next) => {
    req.data.board = await board_queries.find_by_url(req.params.board);
    req.data.user.permissions = await permissions.list_by_user_and_board(req.data.user.id, req.data.board.id);
    req.data.board.permissions = await permissions.list_by_board(req.data.board.id);
    req.data.board.report_reasons = await rule_queries.list_by_board(req.data.board.id);
    req.data.board.reports = await report_queries.list_by_board(req.data.board.id);
    req.data.board.bans = await ban_queries.list_for_mod(+req.data.board.id);
    req.data.possible_permissions = config.permissions;
    res.render("board/edit", req.data);
});

router.post("/:board/edit", async (req, res, next) => {
    let board = await board_queries.find_by_url(req.params.board);
    let permission = await permission_queries.has_permission(req.data.user.id, board.id, "change_settings");
    if (!permission) {
        return res.status(401).send();
    }
    await board_queries.update(board.id, {
        name: req.body.name,
        description: req.body.description,
        namefield: +req.body.namefield,
        international: !!req.body.international,
        worksafe: !!req.body.worksafe,
        hidden: !!req.body.hidden
    });
    res.redirect("back");
});

router.get("/:board/rules", async (req, res) => {
    const board = await board_queries.find_by_url(req.params.board);
    if (!board) {
        return res.status(404).send("");
    }
    req.data.rules = await rule_queries.list_by_board(board.id);
    req.data.layout = false;
    res.render("report_reason_options", req.data);
});

router.get("/:board/stick/:thread", async (req, res) => {
    if (req.data.user.permissions.indexOf("stick_threads") === -1) {
        return res.redirect("back");
    }
    await post_queries.set_thread_stick(req.params.thread, true);
    return res.redirect("back");
});

router.get("/:board/unstick/:thread", async (req, res) => {
    if (req.data.user.permissions.indexOf("stick_threads") === -1) {
        return res.redirect("back");
    }
    await post_queries.set_thread_stick(req.params.thread, false);
    return res.redirect("back");
});

router.delete("/:board", async (req, res) => {
    const board = await board_queries.find_by_url(req.params.board);
    if (board.board_user === req.data.user.id) {
        await board_queries.delete(board.id);
    }
    return res.redirect("/");
})

router.get("/:board/:thread", async (req, res, next) => {
    if (!Number.isInteger(+req.params.thread)) {
        return res.status(404).render("404", req.data);
    }
    let board = await board_queries.find_by_url(req.params.board);
    let thread = await post_queries.find_by_id(req.params.thread);

    if (!board || !thread) {
        return res.status(404).render("404", req.data);
    }
    const replies = await post_queries.list_replies([thread.id], req.data.user.id);
    const reply_ids = replies.map(reply => reply.id);
    const attachments = await post_controller.get_attachments([thread.id, ...reply_ids]);
    const embed_sources = await embed_source_queries.list();
    thread = await post_controller.process_thread(
        thread,
        replies,
        embed_sources,
        attachments,
        board,
        true,
        false
    );
    thread.hide_reply_button = true;
    board.threads = [thread];
    req.data.thread = thread;
    req.data.board = board;
    req.data.supported_formats = config.extensionWhiteList;
    res.render("thread", req.data);
});

router.delete("/:board/:post", async (req, res, next) => {
    const is_admin = req.data.user.permissions.indexOf("admin") !== -1;
    const can_delete = req.data.user.permissions.indexOf("delete_posts") !== -1;
    if (!is_admin && !can_delete) {
        return res.status(403).send("");
    }
    const post = await post_queries.find_by_id(req.params.post);
    const board = await board_queries.find_by_url(req.params.board);
    if (!board || !post) {
        return res.status(404).send("");
    }
    if (board.id !== post.board) {
        return res.status(400).send("");
    }
    await post_queries.delete(post.id);
    res.status(204).send("");
});

module.exports = router;
