const posts = require("../queries/post");
const attachments = require("../queries/attachment");
const {process_attachments} = require("../controllers/post");
const router = require("express-promise-router")();

router.get("/:id", async (req, res) => {
    req.data.post = await posts.find_by_id(req.params.id);
    req.data.post.attachments = await attachments.find_by_post(req.params.id);
    await process_attachments(req.data.post.attachments);
    req.data.layout = false;
    if (!req.data.post) {
        return res.status(404).send("");
    }
    res.render("post/show", req.data);
});

module.exports = router;