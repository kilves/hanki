const express = require("express");
const router = require("express-promise-router")();

router.get(/\/.*(\.png|\.gif|\.ico)/, (req, res, next) => {
    res.status(404);
    res.send("");
});

module.exports = router;
