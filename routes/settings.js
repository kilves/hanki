const express = require("express");
const knex = require("../db").knex;
const router = require("express-promise-router")();

router.get("/", async (req, res, next) => {
    req.data.logins = await knex("logins").where("board_user", req.data.user.id);
    res.render("settings", req.data);
});

module.exports = router;
