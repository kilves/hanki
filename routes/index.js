const express = require("express");
const router = require("express-promise-router")();

/* GET home page. */
router.get("/", function(req, res, next) {
  req.data = {
    ...req.data,
    title: "Pohjoislauta",
  };
  res.render("index", req.data);
});

module.exports = router;
