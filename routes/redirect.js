const post_queries = require("../queries/post");
const board_queries = require("../queries/board");
const permission_queries = require("../queries/permission");
const {not_found} = require("../controllers/error");
const router = require("express-promise-router")();

router.get("/", async (req, res) => {
    if (!req.query.id) {
        return res.status(404).render("404", req.data);
    }
    const post = await post_queries.find_by_id(req.query.id);
    const source = req.query.src.replace(/^http(s*):\/\//, "");
    const [base_url, board_url, post_id] = source.split("/");

    if (!post) {
        return res.status(404).send("");
    }

    const board = await board_queries.find_by_id(post.board);
    const id = post.thread ? post.thread : post.id;
    const url = `${board.url}/${id}#${post.id}`;

    if (board_url === board.url && (post_id === post.id || post_id === post.thread)) {
        return res.status(200).send("S");
    } else {
        return res.status(200).send(`R ${url}`);
    }

});

router.get("/:id", async (req, res) => {
    const post = await post_queries.find_by_id(req.params.id);
    const board = await board_queries.find_by_post_id(req.params.id);
    if (!board || !post) {
        return not_found(req, res);
    }
    if (!(await permission_queries.is_admin(req.data.user.id)) && board.hidden) {
        return not_found(req, res);
    }
    const thread = await post_queries.find_by_id(post.thread);
    if (thread) {
        res.redirect(`/${board.url}/${thread.id}/${post.id}`);
    } else {
        res.redirect(`/${board.url}/${post.id}`);
    }
});

module.exports = router;