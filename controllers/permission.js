const knex = require("../db").knex;

module.exports.list_by_user_and_board = async function(user_id, board_id) {
    let permissions = await knex("permissions")
        .where("board_user", user_id)
        .andWhere("board", board_id);

    return permissions.map(p => p.permission);
};

module.exports.list_by_board = async function(board_id) {
    let board_permissions = await knex("permissions")
        .select("permissions.*", "users.username")
        .where("board", board_id)
        .innerJoin("users", "users.id", "permissions.board_user");

    let user_permissions = {};

    for (let perm of board_permissions) {
        if (!user_permissions[perm.username]) {
            user_permissions[perm.username] = {
                username: perm.username,
                user: perm.board_user,
                permissions: [
                    {permission: "ban", value: false},
                    {permission: "unban", value: false},
                    {permission: "check_reports", value: false},
                    {permission: "change_settings", value: false},
                    {permission: "change_rules", value: false},
                    {permission: "stick_threads", value: false},
                    {permission: "lock_threads", value: false},
                    {permission: "delete_posts", value: false},
                ]
            };
        }
        user_permissions[perm.username].permissions.find(p => p.permission === perm.permission).value = true;
    }
    return Object.values(user_permissions);
};
