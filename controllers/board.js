const permission_queries = require("../queries/permission");

module.exports.populate_permissions = async function(req, board_id) {
    req.data.user.permissions = await permission_queries.list_permissions(req.data.user.id, board_id);
    if (req.data.user.permissions.length > 0) {
        req.data.user.mod = true;
    }
    if (req.data.user.permissions.find(permission => permission.permission === "admin")) {
        req.data.user.admin = true;
    }
}