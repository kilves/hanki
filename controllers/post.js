const knex = require("../db").knex;
const pool = require("../db").pool;
const board_queries = require("../queries/board");
const post_queries = require("../queries/post");
const moment = require("moment");
const embed_source_queries = require("../queries/embed_source");
const Handlebars = require("handlebars");

const HUMAN_READABLE_SIZES = ["B", "KB", "MB", "GB", "TB"];
const ATTACHMENT_TEMPLATES = new Map();

const MESSAGE_REPLACES = [
    [/&/gm, "&amp;"],
    [/"/gm, "&quot;"],
    [/'/gm, "&#39;"],
    [/`/gm, "&#96;"],
    [/\s{16,}/gm, " "],
    [/!/gm, "&#33;"],
    [/@/gm, "&#64;"],
    [/\$g/, "&#36;"],
    [/%/gm, "&#37;"],
    [/\(/gm, "&#40;"],
    [/\)/gm, "&#41;"],
    [/=/gm, "&#61;"],
    [/\+/gm, "&#43;"],
    [/{/gm, "&#123"],
    [/}/gm, "&#125"],
    [/\[b](.+)\[\/b]/gm, "(b)$1(/b)"],
    [/\[i](.+)\[\/i]/gm, "(i)$1(/i)"],
    [/\[u](.+)\[\/u]/gm, "(u)$1(/u)"],
    [/\[code](.+)\[\/code]/gs, "(div class=\"code\")$1(/div)"],
    [/\[/gm, "&#91;"],
    [/]/gm, "&#93"],
    [/\bhttps?:\/\/\S+/gm, "(a href=\"$&\" target=\"blank_\")$&(/a)"],
    [/^(>[^>].+)/gm, "(span class=\"greenquote\")$1(/span)"],
    [/^(<[^>].+)/gm, "(span class=\"bluequote\")$1(/span)"],
    [/>>(\d+)/gm, "(span class=\"reference\")(a href=\"/redirect/$1\")>>$1(/a)(/span)"],
    [/\n/gm, "(br)"],
    [/\s/gm, " "],
    [/</gm, "&lt;"],
    [/>/gm, "&gt;"],
    [/\[v]/gm, " "],
    [/\(/gm, "<"],
    [/\)/gm, ">"]
];

let process_replies = module.exports.process_replies = async function(replies) {
    for (let reply of replies) {
        reply.hide_reply_button = true;
    }
};

let process_attachments = module.exports.process_attachments = async function(attachments) {
    for (let attachment of attachments) {
        attachment.thumb_name = `${attachment.orig_name.split(".").slice(0, -1).join(".")}.${attachment.thumb_ext}`;
    }
}

module.exports.process_threads = async function(threads, user_id=null, board = null) {
    const thread_ids = threads.map(thread => thread.id);
    const replies = await post_queries.list_replies(thread_ids, user_id);
    const reply_ids = replies.map(reply => reply.id);
    const attachments = await get_attachments([...reply_ids, ...thread_ids]);
    const embed_sources = await embed_source_queries.list();
    for (let thread of threads) {
        await module.exports.process_thread(
            thread,
            replies.filter(reply => reply.thread === thread.id),
            embed_sources,
            attachments,
            board
        );
    }
}

function get_embed_frame(embed_sources, post) {
    const embed_source = embed_sources.find(source => post.embed_source === source.id);
    if (!embed_source) {
        return;
    }
    let frame = ATTACHMENT_TEMPLATES.get(embed_source.id);
    if (!frame) {
        frame = Handlebars.compile(embed_source.code);
        ATTACHMENT_TEMPLATES.set(embed_source.id, frame);
    }
    return frame({
        width: embed_source.width,
        height: embed_source.height,
        code: post.embed_code
    });
}

module.exports.process_thread = async function(
    thread,
    replies,
    embed_sources,
    attachments,
    board = null,
    all_messages = false,
    show_expand = true) {

    if (!board) {
        board = await board_queries.find_by_id(thread.board);
        thread.show_location = true;
    }

    if (!board) {
        return;
    }

    for (let attachment of attachments) {
        if (attachment.mime.startsWith("audio/")) {
            attachment.audio = true;
        } else if (attachment.mime.startsWith("video/")) {
            attachment.video = true;
        }
    }

    thread.attachments = attachments.filter(attachment => attachment.post === thread.id);
    await process_attachments(thread.attachments);
    thread.embed = get_embed_frame(embed_sources, thread);

    if (!all_messages) {
        thread.omitted = Math.max(replies.length - 3, 0);
        if (replies.length <= 3) {
            thread.replies = replies;
        } else {
            thread.replies = replies.slice(replies.length - 3);
        }
    } else {
        thread.replies = replies;
    }

    for (let reply of thread.replies) {
        reply.attachments = attachments.filter(attachment => attachment.post === reply.id);
        await process_attachments(reply.attachments);
        reply.embed = get_embed_frame(embed_sources, reply);
        reply.board = board;
    }

    thread.show_expand = !all_messages && show_expand && thread.omitted > 0;
    thread.show_contract = all_messages && show_expand;
    thread.board = board;
    await process_replies(thread.replies);
    return thread;
};

module.exports.get_my_threads = async function(page, user) {
    let threads = await post_queries.list_user_threads(user.id, page);
    await module.exports.process_threads(threads, user.id);
    return { threads, name: "my_threads", collection: true };
};

module.exports.get_replied_threads = async function(page, user) {
    let threads = await post_queries.list_replied_threads(user.id, page);
    await module.exports.process_threads(threads, user.id);
    return { threads, name: "replied_threads", collection: true }
};

module.exports.get_all_threads = async function(page, user) {
    let threads = await post_queries.list_all_threads(user.id, page);
    await module.exports.process_threads(threads, user.id);
    return { threads, name: "ukko_all_threads", collection: true, url: "ukko" }
}

module.exports.get_board_threads = async function(url, page, user) {
    let board = await board_queries.find_by_url(url);
    if (!board) {
        return;
    }

    let threads = await post_queries.list_board_threads(board.id, user.id, page);
    await module.exports.process_threads(threads, user.id, board);
    return { ...board, threads };
};

module.exports.get_thread = async function(thread_id, user_id, full) {
    let thread = await post_queries.find_by_id(thread_id);
    let board = await board_queries.find_by_id(thread.board);
    const replies = await post_queries.list_replies([thread_id], user_id);
    const reply_ids = replies.map(reply => reply.id);
    const attachments = await get_attachments([thread_id, ...reply_ids]);
    const embed_sources = await embed_source_queries.list();
    await module.exports.process_thread(
        thread,
        replies,
        embed_sources,
        attachments,
        board,
        full,
        true
    );
    return thread;
};

let human_size = module.exports.human_size = function (size, suffixes = HUMAN_READABLE_SIZES, max=100) {
    size = +size;
    let i = 0;
    while (size > 1000 && i < max) {
        size /= 1000;
        i++;
    }
    return `${size.toFixed(1)} ${suffixes[i]}`
}

let get_attachments = module.exports.get_attachments = async function(post_ids) {
    const attachments = await pool.fetchAll(`
        SELECT * FROM attachments WHERE post = ANY ($1)
    `, [post_ids]);

    for (let attachment of attachments) {
        attachment.size = human_size(attachment.size);
    }

    return attachments;
};

module.exports.parse_message = function(message) {
    message = message.trim();
    for (let [a, b] of MESSAGE_REPLACES) {
        message = message.replace(a, b);
    }
    return message;
}

module.exports.hide = async function(post_id, user) {
    await knex("hides").insert({
        post: post_id,
        board_user: user.id
    });
};

module.exports.unhide = async function(post_id, user) {
    await knex("hides")
        .where("board_user", user.id)
        .andWhere("post", post_id)
        .delete();
};
