module.exports.error = function(res, code, message = "An error occoured") {
    res.status(code).render("error", { error: { message: message }});
}

module.exports.not_found = function(req, res) {
    res.status(404).render("404", req.data);
}