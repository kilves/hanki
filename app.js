const createError = require("http-errors");
const express = require("express");
const i18n = require("i18n");
const base = require("./base").base;
const hb = require("express-handlebars");
const hbhelpers = require("handlebars-helpers");
const moment = require("moment");
const helpers = hbhelpers();
const path = require("path");
const useragent = require("express-useragent");
const cookieParser = require("cookie-parser");
const logger = require("morgan");

const indexRouter = require("./routes/index");
const boardRouter = require("./routes/board");
const usersRouter = require("./routes/users");
const postsRouter = require("./routes/post");
const rulesRouter = require("./routes/rules");
const redirectRouter = require("./routes/redirect");
const permissionsRouter = require("./routes/permissions");
const singlepostRouter = require("./routes/singlepost");
const reportsRouter = require("./routes/report");
const settingsRouter = require("./routes/settings");
const attachmentsRouter = require("./routes/attachment");
const resourcesRouter = require("./routes/resources");
const banRouter = require("./routes/ban");
const {botDetector} = require("./botDetector");
const {BoardError} = require("./util/error");
const {human_size} = require("./controllers/post");

helpers.t = function(text, context) {
  return i18n.__({phrase: text, locale: context.data.root.language});
};

helpers.duration = function(text) {
  const full_duration = +text;
  const minutes = Math.floor(full_duration / 60);
  const seconds = Math.floor(full_duration - minutes * 60);
  return moment().set({
    minutes: minutes,
    seconds: seconds
  }).format("m:ss");
}

helpers.id3 = function(id3_name, id3_artist, id3_length, id3_bitrate) {
  id3_length = duration(id3_length);
  id3_bitrate = human_size(id3_bitrate, ["b/s", "kb/s", "Mb/s", "Gb/s", "Tb/s"], 1)
  return [id3_name, id3_artist, id3_length, id3_bitrate].filter(e => !!e).join(", ");
}

function duration(text) {
  const full_duration = +text;
  const minutes = Math.floor(full_duration / 60);
  const seconds = Math.floor(full_duration - minutes * 60);
  return moment().set({
    minutes: minutes,
    seconds: seconds
  }).format("m:ss");
}

helpers.url = function(text) {
  return text.replace(/-/g, "%2D");
}

helpers.raw = function(text) {
  return text.replace("\n", "<br/>");
}

helpers.short = function(text) {
  if (!text) {
    return "";
  }
  let short_text = text.substring(0, 40);
  if (short_text.length === 40) {
    short_text += "...";
  }
  return short_text
      .replace(/<[^>]*>/, "")
      .replace("<", "&lt;")
      .replace(">", "&gt;");
};

helpers.time = function(time_string) {
  if (!time_string) {
    return "";
  }
  return moment(time_string).format("DD.MM.YYYY HH:mm:ss");
};

const app = express();
app.enable("trust proxy");

// localization
i18n.configure({
  directory: __dirname + "/locales"
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.engine("handlebars", hb({
  helpers: helpers
}));
app.set("view engine", "handlebars");

app.use(attachmentsRouter);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
let cookieSecret = process.env.COOKIE_SECRET ? process.env.COOKIE_SECRET : "placeholder_secret";
app.use(cookieParser(cookieSecret));
app.use(useragent.express());
app.use(botDetector);
app.use(express.static(path.join(__dirname, "public")));
app.use(logger("dev"));

app.use("/", resourcesRouter);
app.use(i18n.init);
app.use(base);
app.use("/rules", rulesRouter);
app.use("/jsrdr", redirectRouter);
app.use("/redirect", redirectRouter);
app.use("/permissions", permissionsRouter);
app.use("/singlepost", singlepostRouter);
app.use("/post", postsRouter);
app.use("/reports", reportsRouter);
app.use("/settings", settingsRouter);
app.use("/user", usersRouter);
app.use("/ban", banRouter);
app.use("/", boardRouter);
app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = err;

  if (!(err instanceof BoardError)) {
    res.locals.error = new BoardError("Unexpected error occoured", err);
  }

  // render the error page
  res.status(err.status || 500);
  if (req.data) {
    req.data.error = res.locals.error;
  }

  if (err.status === 404) {
    res.render("404", req.data);
  } else {
    console.error(err);
    res.render("error", {error: res.locals.error});
  }
});

module.exports = app;
